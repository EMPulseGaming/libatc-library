# ![logo](https://goo.gl/vDt4po)    
    
[![Build Status](https://travis-ci.org/h2so5/libatc.png?branch=master)](https://travis-ci.org/h2so5/libatc)   

## Introduction    
	
>  **LIBATC** is a Encryption/Decryption Library for C++ and C#/.NET 
>      Part of the [AttacheCase ver.2.8.3.0](https://goo.gl/kA6Nem) project.
> 	
>>   * Decryption for *.atc and *.exe format	 
>>   * Encryption for *.atc format	
>
>	libatc doesn't provide any platform-dependent functions.	
	
## INFORMATION:		
	
   For project Documentation, see:	  
     * [COPYING](COPYING.md) 	
         - Licensing and copyright information.	    
     * [COMPATIBILITY](https://goo.gl/EY7PcW)	   
         - Supported compilers, architectures, etc.	   
     * [INSTALL](doc/INSTALL.md) 	 
         - Instructions on building and installing libatc.       
	 * [How2Use](doc/How2Use.md)		
         - Newer instructions on how to use AttacheCase	      
     * [CHANGELOG](doc/CHANGELOG.md)		
         - Logged Changes for this repo.	 
     * [THANKS](https://goo.gl/EY7PcW)	     
         - Credits and appreciation where due.	    
     * [STYLE](https://goo.gl/EY7PcW)	     
         - Coding style rules (only relevant if you're a contributor).      
	  
## Requirements	for AttacheCase      
* * *	   
     
Operating environment:		
You can use it with Windows XP / Vista / 7 / 8 / 10		
	
However, [.NET Framework 4](https://www.microsoft.com/en-US/download/details.aspx?id=17851) is required for the operation. 		
It must be installed on Windows XP, Vista, 7.		
For Windows 8, 10, you do not need to install anything (installed by default).		
		
As for Windows XP, I think that it will work, since support of Microsoft is terminated, 	
it will not supported.		
		
Read File: [INSTALL](INSTALL.md)   	
     
## Links		
	
  * [libatc](https://goo.gl/5Lh5RM)    	
    - Libatac Library repo		
  * [AttacheCase](https://goo.gl/kA6Nem)		
    - Attache Case ver. 2.x pulled to current repo.		
  * [Attache Case](https://goo.gl/AS7gAZ)		
    - Attache Case ver. 3.x newiest app repo.	
  * [Project Stats](https://qiita.com/hibara)		
    - Hibara's project stats page.		
	
		
	[HomePage](https://hibara.org/)		
    [Home Blog](https://hibara.org/blog/)		
    [Authors Overview -GH](https://github.com/h2so5)	
	
## DOWNLOAD AttacheCase ver.2.8.3.0 release		
- - -		
	
Self executable installer  [atcs2830.exe](https://hibara.org/software/attachecase/atcs2830.exe).	
>  MD5:  	d5e82612ad7c3138d20632fcdafcfb26	
>  SHA-1:  	adf5cbb4cbefa475c8e34c82d8957201eac71e3a	
	
ZIP file  [atcs2830.zip](https://hibara.org/software/attachecase/atcs2830.zip)		
>  MD5:  	ee589b191e71e88d496244814e7b6ebe	
>  SHA-1:   3ce3765927abb5b4b0afa2ee00baafacc596600e	
	
