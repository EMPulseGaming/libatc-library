
## Install

    When installing from installer ( atcs****.exe )		
    Double click "atcs ****. Exe" as it is and follow the instructions of the setup wizard to install it. 		
	I think that it is almost never set by you.		
    When installing from Zip file ( atcs****.zip ) Overwrite all files to the folder that was used before. 		
	When the folder changes, the extension association setting etc. become invalid. 	
	If you want to change the folder, please remove the old AttachéCase once again by the method of the 	
	next paragraph and install it again.	


## Uninstall	
	
    When installed from installer ( atcs****.exe ) Select "AttachéCase" from "Programs and Functions" of 	
	"Control Panel" (Windows XP etc. from "Add / Remove Programs") and delete it. 	
	However, if you want to cancel the association setting with the extension (* .atc), please refer to 	
	the following "How to disassociate" before doing.	
		
	When installed from Zip file ( atcs****.zip )	
    Please delete each folder with the AttachéCase. However, 	
	if you want to cancel the association setting with the extension (* .atc), please refer to the 		
	following "How to disassociate" before doing.		
	