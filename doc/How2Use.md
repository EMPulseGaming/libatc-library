## How to use     
* * *    
			This is moved from updated source doc: [How to use](https://goo.gl/Tpy3Vs)
			so many of these options are not inplace for this repo yet... 
			[Useage](doc/Useage.md) is current how-to, but its rather vague.
			
					<div>

### Encrypt files / folders

					</div>

Usage is simple.
            Just drag and drop the file you want to encrypt on the "AttachéCase" shortcut icon or
            on the window you started up.

					![Drag and Drop files or directories](https://goo.gl/D7rzGa)

Alternatively, if the type of the output file has already been determined in advance,
            you can select the output format first.

					![Select output format first](https://goo.gl/dQKjyS)

After dragging and dropping the file, enter the password in the following window.
            I recommend you to enter a longer password that is hard to guess.

					<div class="alert alert-danger">
						<i class="fa fa-warning">*
            Until the previous version (~ ver.2),
            there was a limit on the number of characters to enter the password,
            it was within 32 characters, but in ver.3 and later There is no restriction.
					</i></div>

					![Password input window](https://goo.gl/8r4w1a)

Also on this window,
            you can select the output format by clicking on the AttachéCase icon.

					![Click on the Attachment Case icon](https://goo.gl/uyCNB6)

					![Select output format later](https://goo.gl/GZcDpv)

Once you have entered your password, press the OK button.
            You will be prompted to enter the same password again for confirmation.
            This is to prevent encryption with an incorrect password, which can not be restored.

					![Password re-enter window](https://goo.gl/4ccnnk)

Next, press "Encrypt" button to start the process,
            and when the message "Complete" appears, an encrypted file is created.

					![Encryption completion window](https://goo.gl/VLxSHC)
					![ATC file icon](https://goo.gl/DnVZjR)

					<div>

### Decrypt the file

					</div>

As with encryption,
            drag & drop encrypted file on the icon of the AttachéCase or open window.。

					![Drag and Drop the encrypted file](https://goo.gl/RZCXcA)

Even if the extension is different in the AttachéCase file,
            it directly looks at the file data and automatically determines whether it is an encrypted file or not.
            So, even if you drag and drop a self-executable file, you can decrypt it without problems.

If you could confirm that it is an encrypted file,
            you will be asked for a password to decrypt as follows.

					![Password input window for decryption](https://goo.gl/tXECRM)

When the "completion" appears, the original file should be in the same place.

					![Decryption completion window](https://goo.gl/ivpb47)
					![Word file icon](https://goo.gl/cVmhnq)

					<div>

### Output self-executing format

					</div>

					![select self-executable format](https://goo.gl/CrwkKE)

When you select the following icon on the screen above,
            before encryption and after dragging and dropping files / folders,
            it is outputted as an executable file (* .exe).

![You can do what you can](https://goo.gl/qmgHmK)

#### Notes on self-executable output

*   <i class="fa fa-check">***The environment to run requires
              [.NET Framework 4](https://www.microsoft.com/en-US/download/details.aspx?id=17851)**  

              Even in the environment where the AttachéCase is not installed, the above framework is necessary.</i>
*   <i class="fa fa-check">***The file size increases.**  

              Because the function to decrypt is included in executable file,
              the file size will be larger than the ordinary encrypted file (* .atc).</i>
*   <i class="fa fa-check">***When outputting in executable format, options you set in the AttachéCase are not saved.**  

              In other words, basic setting, save setting, delete setting, system setting etc are not included in the file,
              so it is necessary to be careful.
              It is just a simple function to decrypt & decompress.
              For example, the password is always asked,
              and the decrypted file to be saved is the location of the EXE file,
              and the original file (*.exe) is not deleted.</i>
*   <i class="fa fa-check">***There is no particular distribution condition etc. of the output executable file.**  

              Distribution of executable files is free to go.
              For files output by the AttachéCase, you can do it freely,
              including regular *.atc files.
              ( -> Please refer to "[FAQ](../support/#faq)" and
              "[License](../support/#license)" for details.)</i>

					<div class="heading heading-primary heading-border heading-bottom-border">

## Command line options at startup

					</div>

There is nothing to do with the user who does not know what "command line option at startup".
            Please skip this chapter.

This chapter explains how to customize it for those who want to use the AttachéCase more highly.

In the AttachéCase, it is possible to perform encryption /
            decryption processing by passing arguments at startup.
            It can be handled from other applications, and it can be operated from batch file etc.
            For example, it may be convenient to use encryption /
            decryption by creating a shortcut of the Attache case and writing it in the "link destination",
            by using different passwords.

					<div class="alert alert-danger">
						<i class="fa fa-warning">*
            In the command line argument,
            password string etc will be recorded and displayed as it is without being hidden,
            so please set carefully. To the end this is also your self-responsibility.
					</i></div>

#### Notes on startup command line option

*   <i class="fa fa-check">*Especially when no argument is specified,
              the AttachéCase will be activated with the already set contents.
              For example, if you set "After decryption open the associated files or folders" usually ON,
              folders will be opened after decryption unless you specifically specify OFF as argument.</i>
*   <i class="fa fa-check">*The case of the argument is not distinguished. However,
              the contents of the setting such as the password are distinguished.</i>
*   <i class="fa fa-check">*If you put control characters such as blanks and Tab between the argument and "=" (Ecor),
              it will not be recognized correctly.</i>
*   <i class="fa fa-check">*If you put a blank in the password of the setting contents or if the file path contains spaces,
              you can recognize it correctly by enclosing the whole argument with "" (double quotation).</i>
*   <i class="fa fa-check">*If it is executed with command line argument specified,
              it will not be saved when exiting.
              Even if you set the operation again when canceling processing by user cancellation etc,
              it will not be reflected.</i>
*   <i class="fa fa-check">*You can not specify an encrypted file in the executable (*.exe) file output
              by the AttachéCase on the command line. You can only do to the main body (AtchCase.exe).</i>
*   <i class="fa fa-check">*All the setting items of the AttachéCase can not be specified
              with the command line option (some of them can not be set).</i>

					<div>

### Basic Option

					</div>

					`/p=[PASSWORD]`  

					Password  

					e.g.) /p=hirakegoma *When the path contains spaces... e.g.) "/p=hirake goma"  

					`/mempexe=[0,1]`  

            Execute without confirmation with memory password.  

					e.g.) /mempexe=1 *When you want to confirm without doing it... e.g.) /mempexe=0

					`/wmin=[0,1]`  

            Always process windows minimized  

					e.g.) /wmin=1 *If you do not want to minimize and always perform... e.g.) /wmin=0

					`/tskb=[0,1]`  

            Do not display on the task bar when window is minimized.  

					e.g.) /tskb=1 *When you want to display... e.g.) /tskb=0

					`/tsktr=[0,1]`  

            Display icon in task tray  

					e.g.) /tsktr=1 *When you do not want to display icons... e.g.) /tsktr=0

					<div class="alert alert-warning alert-dismissible" role="alert">
						<i class="fa fa-warning">*Please note  

            It is not possible to turn off (= 0) both "Do not show on taskbar
            when window is minimized" option (/ tskb) and "Display icon on task tray" option (/ tsktr)
            becouse you could not operate the AttachéCase. However,
            if both options are set, **"Force icon on task tray"** is forced ON.
					</i></div>

					`/opd=[0,1]`  

            For folders, open after decryption.  

					e.g.) /opd=1 *If you do not want to open the folder... e.g.) /opd=0

					`/opf=[0,1]`  

            Open the decrypted file with the associated software.  

					e.g.) /opf=1 *If you do not need to open the file... e.g.) /opf=0

					`/exit=[0,1]`  

            After processing, exit the AttachéCase  

					e.g.) /exit=1 *If you do not want to exit... e.g.) /exit=0

					`/front=[0,1]`  

            Display the window at the front of the desktop.  

					e.g.) /front=1 *If you do not want it to be the frontmost... e.g.) /front=0

					`/nohide=[0,1]`  

            Enter the password without hiding it with "*"  

					e.g.) /nohide=1 *If you want to hide and enter... e.g.) /nohide=0

					`/exeout=[0,1]`  

            Always output in self-executable file.  

					e.g.) /exeout=1 ※When you do not want to output in executable file... e.g.) /exeout=0

					`/chkexeout=[0,1]`  

            Display the checkbox on the main form.  

					e.g.) /chkexeout=1 *If you do not want to display a check box... e.g.) /chkexeout=0

					`/askende=[0,1]`  

            Query for encryption / decryption processing.  

					e.g.) /askende=1 *When I do not want to query... e.g.) /askende=0

					`/en=1`  

            Explicitly specify encryption.  

					*When this option is specified, it shifts to "encryption" processing at startup.   

            Even if query option is specified, it is ignored.

					`/de=1`  

            Explicitly specify decryption.  

            *When this option is specified, it shifts to "decryption" processing at startup.   

            Even if query option is specified, it is ignored.

					`/nomulti=[0,1]`  

            Do not start multiple.  

					e.g.) /nomulti=1 *When starting multiple... e.g.) /nomulti=0

					<div>

### Save Option

					</div>

					`/saveto=[PATH]`  

            Always save the encrypted file in the same place.  

					e.g.) /saveto=MyDirPath  

					*If the path contains spaces...  

					"/saveto=C:\Documents and Settings\User1\My Documents"

					`/dsaveto=[PATH]`  

            Always decrypt the file to the same place.  

					e.g.) /dsaveto=MyDirPath  

          *If the path contains spaces...  

					"/saveto=C:\Documents and Settings\User1\My Documents"

					`/ow=[0,1]`  

            In the case of the same name file, overwrite it without confirmation.  

					e.g.) /ow=1 *If you do not confirm overwrite... e.g.) /ow=0  

					`/orgdt=[0,1]`  

            Adjust the time stamp of the encrypted file to the original file.  

					e.g.) /orgdt=1 *When keeping the time stamp as it was at the time of generation... e.g.) /orgdt=0

					`/now=[0,1]`  

            Set the timestamp of the decrypted file to the creation date.  

					e.g.) /now=1 *If you do not change the original timestamp... e.g.) /now=0

					`/allpack=[0,1]`  

            Multiple files are combined into one encrypted file.  

					e.g.) /allpack=1 ※If you do not combine into one... e.g.) /allpack=0

					`/oneby=[0,1]`  

            Encrypt / decrypt files individually in folders.  

					e.g.) /oneby=1 ※When not processing individually... e.g.) /oneby=0

					`/nopfldr=[0,1]`  

            Do not generate parent folder when decrypting.  

					e.g.) /nopfldr=1 *When generating a parent folder when decrypting... e.g.) /oneby=0

					`/withext=[0,1]`  

					Include extension in encrypted file name

					ex). /withext=1 ※If not included, ex). /withext=0

					`/autoname=[FORMAT]`  

            Automatically add name to encrypted file name.

> e.g.) /autoname=<filename>_<date:yymmdd><ext>
> 
> 								*If "/autoname" is not specified, this function is turned off.
> 
> 								*If the path contains spaces...
> 
> 								ex). "/autoname=<filename>_<date:yymmdd><ext>"

						`/zipsaveto=[PATH]`  

            Always save the file in the same place with the password zip file.  

						e.g.) /zipsaveto=MyData  

            *If the path contains spaces...  

						"/saveto=C:\Documents and Settings\User1\My Documents"

						`/zipalgo=[0,1,2]`  

            Cryptographic algorithm of ZIP file with password.  

						0: PkzipWeak, 1: WinZipAes128, 2: WinZipAes256  

						e.g.) /zipalgo=1  

					<div>

### Delete Option

					</div>

					`/del=[0,1,2,3]`  

            After encrypting, delete the original file.  

						0: Do not delete  

						1: Normal deletion  

						2: Delete to trash box  

						3: Complete deletion  

					e.g.) /del=1 *If you do not want to delete the original file,... e.g.) /del=0

					`/delenc=[0,1,2,3]`  

            After decrypting, delete the encrypted file.  

            0: Do not delete  

            1: Normal deletion  

            2: Delete to trash box  

            3: Complete deletion  

					e.g.) /delenc=1 *If you do not want to delete the encrypted file,... e.g.) /delenc=0

					`/chkdel=[0,1]`  

            Display the checkbox on the main form.  

					e.g.) /chkdel=1 ※If you do not want to display a check box,... e.g.) /chkdel=0

					`/delrand=[0-99]`
          How many times to erase a random number.  

					0-99: Specify the number of write operations.  

					e.g.) /delrand=3 ※If you do not write random numbers,... e.g.) /delrand=0

					`/delnull=[0-99]`
          How many times to erase NULL.  

          0-99: Specify the number of write operations.  

					e.g.) /delnull=3 ※If you do not write NULL,... e.g.) /delnull=0

					<div>

### Compression Option

					</div>

					`/comprate=[0-9]`  

          Compression Option(Compression ratio)  

					0-9: Specify the compression ratio.  

					e.g.) /comprate=6 ※When not compressing,... e.g.) /comprate=0

					<div>

### Advanced Option

					</div>

					`/pf=[0,1]`  

          Allow password to use file  

					e.g.) /pf=1 ※If you do not allow,... e.g.) /pf=0

					`/pfile=[PATH]`  

          Password file path for encryption  

					e.g.) /pfile=C:\Temp\pass.txt  

          *If the path contains spaces...  

					"/pfile=C:\Documents and Settings\User1\My Documents\pass.txt"

					`/dpfile=[PATH]`  

          Password file path for decryption  

					e.g.) /dpfile=C:\Temp\pass.txt  

          *If the path contains spaces...  

					"/dpfile=C:\Documents and Settings\User1\My Documents\pass.txt"

					`/nomsgp=[0,1]`  

          Do not display an error if there is no password file.  

					e.g.) /nomsgp=1 *When you display an error,... e.g.) /nomsgp=0

					`/camoext=[EXT]`
          Camouflage in extension of encrypted file.  

					e.g.) /camoext=.jpg *If you do not camouflage,... e.g.) /camoext=null

					<div>

### Other(from the command line only)

					</div>

					`/4gbok=[0,1]`  

          Do not display a warning message even if the output file exceeds 4 GB in self-executable format.  

					*This option is ignored except for self-executable output.  

					e.g.) /4gbok=1 *When displaying,... e.g.) /4gbok=0

					`/ws=[0,1,2]`  

          Specify the state of the window at startup.  

					0: Normal  

					1: Minimize  

					2: Maximize  

					e.g.) /ws=1 *In this case, the window is activated in the "minimized" state.

					`/list=[PATH]`  

          Specify file list to be encrypted / decrypted with different text file.  

					e.g.) /list=C:\Temp\list.txt  

          *If the path contains spaces...  

					"/list=C:\Documents and Settings\User1\My Documents\list.txt"  

          The format of the specified file list is 1 line per file path.  

					e.g.) The contents of "list.txt" specified in the above path↓  

					hogehoge.bmp  

					hogehoge1.bmp  

					hogehoge2.bmp  

					hogehoge3.bmp  

					tmp\temporary.htm  

					C:\Documents and Settings\User1\My Documents\test.doc

> *Even if you put the file path directly into the execution argument separately, you can use it together.
> 
> 					 (Rather than, it will be processed together.)
> 
> 					*Naturally, if there are unexistent paths, an error will occur during processing.

* * *

            Looking at this way, argument specification is quite complicated,
            but I think that it is convenient to use the existing setting as it is,
            just thinking to designate the argument temporarily just want to change.

           For example,  

					`"C:\Program Files\Attache Case\AtchCase.exe" Project1.doc /p=foobar /ow=1 /exit=1 /saveto=encrypt`  

					given that,

            For encrypting "Project1.doc", the password is "foobar",
            no confirmation of overwriting, end after processing,
            save destination is "encrypt" folder.
            For any other unspecified behavior settings, the existing settings are applied.
			
