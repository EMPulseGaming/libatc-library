## Usage

* * *

### Build

#### Windows (C++ and C#)
Open  __libatc/msvc/libatc.sln__

#### Mac OS X
Open  __libatc/xcode/libatc.xcodeproj__

#### Linux (GCC)

- - - -

You need zlib header and binary.

    ```bash
    $ cd libatc
    $ make
    $ make test
    ```

### Decryption

    ``` cpp

	#include <iostream>
	#include <fstream>

	// Using boost for directory operations
	#include <boost/filesystem.hpp>

	#include "ATCUnlocker.h"

	using namespace std;
	using namespace boost::filesystem;

	int main()
	{
		char key[ATC_KEY_SIZE] = "This is a pen.";
		char atc_filename[] = "test.atc";

		ATCUnlocker unlocker;
		ifstream ifs(atc_filename, ifstream::binary);

		ATCResult result = unlocker.open(&ifs, key);
		if (result == ATC_OK) {
		
			// Get the number of files and directories
			int length = unlocker.getEntryLength();

			for (int i = 0; i < length; ++i) {
			
				// Get entry inforamation
				ATCFileEntry entry;
				unlocker.getEntry(&entry, i);

				// Is it a directory?
				if (entry.size == -1) {
				
					// Create a directory
					if (!exists(entry.name_sjis)) {
						create_directory(entry.name_sjis);
					}
					
				} else {
				
					// Write extracted data
					ofstream ofs(entry.name_sjis, ios::binary);
					unlocker.extractFileData(&ofs, &ifs, entry.size);
					
				}
			}
		} else if (result == ATC_ERR_WRONG_KEY) {
			cout << "Error: Wrong password." << endl;
		} else {
			cout << "Error: Cannot open." << endl;
		}
		
		unlocker.close();
		
		return 0;
	}

    ```

### Encryption

    ``` cpp

	#include <iostream>
	#include <fstream>
	#include <ctime>
	#include <zlib.h>

	#include "ATCLocker.h"

	using namespace std;

	int main()
	{
		char key[ATC_KEY_SIZE] = "This is a pen.";
		char atc_filename[] = "test.atc";

		ATCLocker locker;

		// Set the options before open()
		locker.set_compression_level(Z_BEST_COMPRESSION);
		locker.set_passwd_try_limit(10);
		locker.set_self_destruction(false);
		locker.set_create_time(time(NULL));

		ofstream ofs(atc_filename, ifstream::binary);
		locker.open(&ofs, key);

		// Add "dir/"
		{
			ATCFileEntry entry;
				entry.attribute = 16;
				entry.size = -1;
				entry.name_sjis = "dir\\";
				entry.name_utf8 = "dir\\";
				entry.change_unix_time = time(NULL);
				entry.create_unix_time = time(NULL);

			locker.addFileEntry(entry);
		}

		// Prepare input files
		ifstream ifs_foo_txt("foo.txt", ifstream::binary);
			ifs_foo_txt.seekg(0, ios::end);
			size_t foo_txt_size = ifs_foo_txt.tellg();
			ifs_foo_txt.seekg(0, ios::beg);

		ifstream ifs_bar_txt("bar.txt", ifstream::binary);
			ifs_bar_txt.seekg(0, ios::end);
			size_t bar_txt_size = ifs_bar_txt.tellg();
			ifs_bar_txt.seekg(0, ios::beg);


		// Add "dir/foo.txt"
		{
			ATCFileEntry entry;
				entry.attribute = 0;
				entry.size = foo_txt_size;
				entry.name_sjis = "dir\\foo.txt";
				entry.name_utf8 = "dir\\foo.txt";
				entry.change_unix_time = time(NULL);
				entry.create_unix_time = time(NULL);

			locker.addFileEntry(entry);
		}

		// Add "dir/bar.txt"
		{
			ATCFileEntry entry;
				entry.attribute = 0;
				entry.size = bar_txt_size;
				entry.name_sjis = "dir\\bar.txt";
				entry.name_utf8 = "dir\\bar.txt";
				entry.change_unix_time = time(NULL);
				entry.create_unix_time = time(NULL);

			locker.addFileEntry(entry);
		}

		// Write the header
		locker.writeEncryptedHeader(&ofs);

		// Write the file data in the same order as addFileEntry()
		locker.writeFileData(&ofs, &ifs_foo_txt, foo_txt_size);
		locker.writeFileData(&ofs, &ifs_bar_txt, bar_txt_size);
		
		locker.close();
		
		return 0;
	}
	 
    ```
	
* * *

 Read File: [How2Use](How2Use.md) for updated usage.